<?php
class ControllerExtensionModuleShortpanel extends Controller {
	private $error = array();	

	public function index() {
		$data['oc_licensing_home'] = 'https://www.mikadesign.co.uk/store/'; 
		$data['extension_id'] = 30545;   
		$admin_support_email = 'support@mikadesign.co.uk';

		$this->load->language('oc_licensing/oc_licensing');
		
		$data['regerror_email'] = $this->language->get('regerror_email');
		$data['regerror_orderid'] = $this->language->get('regerror_orderid');
		$data['regerror_noreferer'] = $this->language->get('regerror_noreferer');
		$data['regerror_localhost'] = $this->language->get('regerror_localhost');
		$data['regerror_licensedupe'] = $this->language->get('regerror_licensedupe');
		$data['regerror_quote_msg'] = $this->language->get('regerror_quote_msg');
		$data['license_purchase_thanks'] = sprintf($this->language->get('license_purchase_thanks'), $admin_support_email);
		$data['license_registration'] = $this->language->get('license_registration');
		$data['license_opencart_email'] = $this->language->get('license_opencart_email');
		$data['license_opencart_orderid'] = $this->language->get('license_opencart_orderid');
		$data['check_email'] = $this->language->get('check_email');
		$data['check_orderid'] = $this->language->get('check_orderid');
		$data['server_error_curl'] = $this->language->get('server_error_curl');		
		
		if(isset($this->request->get['emailmal'])){
			$data['emailmal'] = true;
		}

		if(isset($this->request->get['regerror'])){
		    if($this->request->get['regerror']=='emailmal'){
		    	$this->error['warning'] = $this->language->get('regerror_email');
		    }elseif($this->request->get['regerror']=='orderidmal'){
		    	$this->error['warning'] = $this->language->get('regerror_orderid');
		    }elseif($this->request->get['regerror']=='noreferer'){
		    	$this->error['warning'] = $this->language->get('regerror_noreferer');
		    }elseif($this->request->get['regerror']=='localhost'){
		    	$this->error['warning'] = $this->language->get('regerror_localhost');
		    }elseif($this->request->get['regerror']=='licensedupe'){
		    	$this->error['warning'] = $this->language->get('regerror_licensedupe');
		    }
		}

		$domainssl = explode("//", HTTPS_SERVER);
		$domainnonssl = explode("//", HTTP_SERVER);
		$domain = ($domainssl[1] != '' ? $domainssl[1] : $domainnonssl[1]);
		echo "<script>console.log($domain)</script>";
		$domain = "demo03.kpis.vn/admin/";

		$data['licensed'] = @file_get_contents($data['oc_licensing_home'] . 'licensed.php?domain=' . $domain . '&extension=' . $data['extension_id']);

		if(!$data['licensed'] || $data['licensed'] == ''){
			if(extension_loaded('curl')) {
		        $post_data = array('domain' => $domain, 'extension' => $data['extension_id']);
		        $curl = curl_init();
		        curl_setopt($curl, CURLOPT_HEADER, false);
		        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
		        curl_setopt($curl, CURLOPT_USERAGENT,'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17');
		        $follow_allowed = ( ini_get('open_basedir') || ini_get('safe_mode')) ? false : true;
		        if ($follow_allowed) {
		            curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
		        }
		        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 9);
		        curl_setopt($curl, CURLOPT_TIMEOUT, 60);
		        curl_setopt($curl, CURLOPT_AUTOREFERER, true); 
		        curl_setopt($curl, CURLOPT_VERBOSE, 1);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);
		        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
		        curl_setopt($curl, CURLOPT_FORBID_REUSE, false);
		        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		        curl_setopt($curl, CURLOPT_URL, $data['oc_licensing_home'] . 'licensed.php');
		        curl_setopt($curl, CURLOPT_POST, true);
		        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($post_data));
		        $data['licensed'] = curl_exec($curl);
		        curl_close($curl);
		    }else{
		        $data['licensed'] = 'curl';
		    }
		}

		$data['licensed_md5'] = md5($data['licensed']);

		$this->load->language('extension/module/shortpanel');
		$this->load->model('localisation/language');
		$this->document->setTitle(strip_tags($this->language->get('heading_title')));
		
		$data['shortpanel_content'] = array('maxlength' => 'text');
		
		$language_settings = array();
        foreach ($data['shortpanel_content'] as $key => $value) {
            $language_settings[$key] = $this->language->get('text_settings_' . $key);
        }
        $data['language_settings'] = $language_settings;		
				
 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';			
		} 		
		
  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
   		);

   		$data['breadcrumbs'][] = array(
       		'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true)
   		);
		
		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/shortpanel', 'user_token=' . $this->session->data['user_token'], true)
   		);		
		
		$data['user_token'] = $this->session->data['user_token'];
		
		// Module
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $data['module_shortpanel'] = $this->request->post['module_shortpanel'];
        } else {
            $data['module_shortpanel'] = $this->config->get('module_shortpanel');
        }		
		
		// Status
		if (isset($this->request->post['module_shortpanel_status'])) {
			$data['module_shortpanel_status'] = $this->request->post['module_shortpanel_status'];
		} else {
			$data['module_shortpanel_status'] = $this->config->get('module_shortpanel_status');		
		}
		
		// Summernote			
		if (isset($this->request->post['module_shortpanel_sn'])) {
			$data['module_shortpanel_sn'] = $this->request->post['module_shortpanel_sn'];
		} else {
			$data['module_shortpanel_sn'] = $this->config->get('module_shortpanel_sn');
		}	
		
		// Ellipsis
		if (isset($this->request->post['module_shortpanel_ellipsis'])) {
			$data['module_shortpanel_ellipsis'] = $this->request->post['module_shortpanel_ellipsis'];
		} else {
			$data['module_shortpanel_ellipsis'] = $this->config->get('module_shortpanel_ellipsis');
		}						
		
		// Replace
		if (isset($this->request->post['module_shortpanel_replace'])) {
			$data['module_shortpanel_replace'] = $this->request->post['module_shortpanel_replace'];
		} else {
			$data['module_shortpanel_replace'] = $this->config->get('module_shortpanel_replace');
		}		
		
		// Position			
		if (isset($this->request->post['module_shortpanel_field'])) {
			$data['module_shortpanel_field'] = $this->request->post['module_shortpanel_field'];
		} else {
			$data['module_shortpanel_field'] = $this->config->get('module_shortpanel_field');
		}	
		
		// Options
		if (isset($this->request->post['module_shortpanel_options'])) {
			$data['module_shortpanel_options'] = $this->request->post['module_shortpanel_options'];
		} else {
			$data['module_shortpanel_options'] = $this->config->get('module_shortpanel_options');
		}
		
		// Custom CSS
		if (isset($this->request->post['module_shortpanel_css'])) {
			$data['module_shortpanel_css'] = $this->request->post['module_shortpanel_css'];
		} else {
			$data['module_shortpanel_css'] = $this->config->get('module_shortpanel_css');
		}		
				
		// styles and scripts
		$this->document->addScript('view/javascript/md_bootstrap_switch/js/shortpanel-switch.min.js');
        $this->document->addStyle('view/javascript/md_bootstrap_switch/css/shortpanel-switch.css');
		$this->document->addStyle('view/stylesheet/shortpanel.css');   		
		
		$data['action'] = $this->url->link('extension/module/shortpanel/save', 'user_token=' . $this->session->data['user_token'], true);
		
		$data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true);
		
		$data['css'] = '';

		$file = DIR_CATALOG . 'view/theme/default/stylesheet/shortpanel.css';

		if (file_exists($file)) {
			$data['css'] = file_get_contents($file, FILE_USE_INCLUDE_PATH, null);
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');		

		$this->response->setOutput($this->load->view('extension/module/shortpanel', $data));
	}
	
	public function save() {
		$this->load->language('extension/module/shortpanel');
		
		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_setting_setting->editSetting('module_shortpanel', $this->request->post);			
			$value = $this->request->post['css'];
			$file = DIR_CATALOG . 'view/theme/default/stylesheet/shortpanel.css';
			$handle = fopen($file, 'w+');
			fwrite($handle, html_entity_decode($value));
			fclose($handle);
			$this->session->data['success'] = $this->language->get('text_success');
		}			
		$this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=module', true));
	}
	
	public function install() {		
		$this->load->model('extension/shortpanel');
        $this->model_extension_shortpanel->install();		
		
		// initial variable
		  $initial = array();
		  $initial['module_shortpanel'] = array(               
			  'maxlength' => 500
		  );

		  $this->load->model('setting/setting');
		  $this->model_setting_setting->editSetting('module_shortpanel', $initial);
		  
		if (empty($installed)) {
		$this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'shortpanel', 'module_shortpanel_sn', '1', '0')");
		$this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'shortpanel', 'module_shortpanel_ellipsis', '1', '0')");
		$this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'shortpanel', 'module_shortpanel_replace', '1', '0')");
		$this->db->query("INSERT INTO `" . DB_PREFIX . "setting` (`setting_id`, `store_id`, `code`, `key`, `value`, `serialized`) VALUES (NULL, '0', 'shortpanel', 'module_shortpanel_css', '1', '0')");
		}
	}
	
	public function uninstall() {
		$this->load->model('extension/shortpanel');
        /*$this->model_extension_shortpanel->uninstall();*/
		$this->load->model('setting/setting');
        $this->model_setting_setting->deleteSetting('shortpanel');
	}							

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'extension/module/shortpanel')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
}
?>