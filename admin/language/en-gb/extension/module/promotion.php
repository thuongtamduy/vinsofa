<?php
$_['heading_title']               = 'Promotions';
$_['heading_title_all_promos']    = 'All Promotions';

$_['error_permission']            = 'Warning: You do not have permission to modify module Promotions module!';
$_['text_success']                = 'Success: You have modified module Promotions module!';
$_['text_enabled']                = 'Enabled';
$_['text_disabled']               = 'Disabled';
$_['button_cancel']			   = 'Cancel';
$_['save_changes']				= 'Save changes';
$_['text_default']				= 'Default';
$_['text_edit']					= 'Edit';
$_['text_module']				 = 'Module';
// Control Panel
$_['entry_code']                  = 'Promotions module status: ';
$_['entry_code_help'] 			 = 'Enable or disable the module';
$_['text_content_top']			= 'Content Top';
$_['text_content_bottom']		 = 'Content Bottom';
$_['text_column_left']			= 'Column Left';
$_['text_column_right']		   = 'Column Right';
$_['entry_layout']        		= 'Layout:';
$_['entry_position']      		  = 'Position:';
$_['entry_status']        		= 'Status:';
$_['entry_sort_order']    		= 'Sort Order:';
$_['entry_action_options']		= 'Actions:';
$_['entry_layout_options']        = 'Layout Options:';
$_['entry_position_options']      = 'Position Options:';
$_['button_add_module'] 		   = 'Add module';
$_['button_remove']			   = 'Remove';

// Licensing
$_['text_your_license']         = 'Your License';
$_['text_please_enter_the_code']= 'Please enter your product purchase license code:';
$_['text_activate_license']     = 'Activate License';
$_['text_not_having_a_license'] = 'Not having a code? Get it from here.';
$_['text_license_holder']       = 'License Holder';
$_['text_registered_domains']   = 'Registered domains';
$_['text_expires_on']           = 'License Expires on';
$_['text_valid_license']        = 'VALID LICENSE';
$_['text_manage']               = 'manage';
$_['text_get_support']          = 'Get Support';
$_['text_community']            = 'Community';
$_['text_ask_our_community']    = 'We have a big community. You are free to ask it about your issue on the forum.';
$_['text_browse_forums']        = 'Browse forums';
$_['text_tickets']              = 'Tickets';
$_['text_open_a_ticket']        = 'Want to comminicate one-to-one with our tech people? Then open a support ticket.';
$_['text_open_ticket_for_real'] = 'Open a ticket';
$_['text_pre_sale']             = 'Pre-sale';
$_['text_pre_sale_text']        = 'Have a brilliant idea for your webstore? Our team of developers can make it real.';
$_['text_bump_the_sales']       = 'Bump the sales';

//GENERAL helpers
$_['text_status'] = 'Status:';

$_['text_sort_order'] = 'Sort Order:';
$_['helper_sort_order'] = 'Set the sort order. If multiple promotions are applied the one with the lowest sort order will be applied first. (For example 1 will be applied before 2)';

$_['text_stackable'] = 'Stackable: ';
$_['helper_stackable'] = 'If set to yes, then the promotion can be used in combination with other promotions in your store. ';


$_['text_promo_name'] = 'Promotion name:';
$_['helper_promo_name'] = 'Choose a promotion name, which will be visible as order total (order details table).';

$_['text_module_name'] = 'Module name:';
$_['helper_module_name'] = 'Choose a module name, which will be used only in the administration of the module.';

$_['text_start_date'] = 'Start date:';
$_['helper_start_date'] = 'Enter the starting date of the promotion. If both the starting and the ending date are empty the promotion will be always active.';

$_['text_end_date'] = 'End date:';
$_['helper_end_date'] = 'Enter the ending date of the promotion. If both the starting and the ending date are empty the promotion will be always active.';

$_['text_total_uses'] = 'Max allowed uses:';
$_['helper_total_uses'] = 'Sets total allowed uses of the promotion.';

$_['text_user_total_uses'] = 'Max uses per customer:';
$_['helper_user_total_uses'] = 'How many times can a customer make use of the promotion.';

$_['text_promo_type'] = 'Promotion type:';
$_['helper_promo_type'] = 'Automatic promotion will apply once the user meets all conditions, whereas Coupon promotion would require the user to satisfy all conditions and use a coupon code.';

$_['text_coupon_code'] = 'Coupon code:';
$_['helper_coupon_code'] = 'Enter a coupon code which would trigger the promotion.';

$_['text_need_login'] = 'Require login:';
$_['helper_need_login'] = 'The promotion will be applicable only to registered and logged in users.';

$_['text_customer_groups'] = 'Customer groups:';
$_['helper_customer_groups'] = 'Choose which customer groups can apply for the promotions.';

$_['text_stores'] = 'Stores:';


?>