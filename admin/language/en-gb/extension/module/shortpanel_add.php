<?php

$_['text_short_description'] 	= 'input short description ( Max Length %s )';

$_['entry_short_description'] 	= '<a href="index.php?route=extension/module/shortpanel&user_token=%s" target="_blank" title="configure module"><i class="fa fa-cog"></i> Configure Module </a><br> Short Description <br>( Max Length %s )';
$_['entry_disabled']			= 'The Product Short Description module is Disabled or has not been Installed <a href="index.php?route=marketplace/extension&type=module&user_token=%s" target="_blank" title="configure module"><i class="fa fa-cog"></i> Configure Module </a>';
