<?php
// Heading
$_['text_warehouse']              = 'Warehouse Management';

$_['text_reset']  = 'Reset';
$_['text_sumup']  = 'Sum & Add To Main Qty'; 

$_['entry_warehouse'] 	  = "Assign individual warehouse quantity: <br><br>";
$_['entry_warehouse_qty'] = "Enter qty";

$_['button_warehouseorder']		 = "Warehouse Stock";
$_['text_selectwarehouse_warehouseorder']       = 'Warehouse stock reduction for order';
$_['column_image']					 = 'Image';
$_['column_product']				 = "Product Name";
$_['column_quantity']       		 = 'Qty Ordered';
$_['column_quantityavailable']       = 'Qty Available';
$_['column_selectwarehouse']       	 = 'Qty Debited';