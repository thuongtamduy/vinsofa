/**
 * @license Copyright (c) 2003-2015, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.enterMode = CKEDITOR.ENTER_BR; 
    config.shiftEnterMode = CKEDITOR.ENTER_P;

	config.filebrowserBrowseUrl = 'index.php?route=common/filemanager';
	//config.filebrowserImageBrowseUrl = 'index.php?route=common/filemanager';
	//config.filebrowserFlashBrowseUrl = 'index.php?route=common/filemanager';
	//config.filebrowserUploadUrl = 'index.php?route=common/filemanager';
	//config.filebrowserImageUploadUrl = 'index.php?route=common/filemanager';
	//config.filebrowserFlashUploadUrl = 'index.php?route=common/filemanager';
	
	//config.filebrowserWindowWidth = '960';
	//config.filebrowserWindowHeight = '580';

	config.extraPlugins = 'codemirror,fontawesome';
	config.removePlugins = 'wsc,scayt,preview,newpage,save,print,language,bt_table';
	
	config.htmlEncodeOutput = false;
	config.entities = false;
	config.startupOutlineBlocks = true;
	config.extraAllowedContent = '*{*}';
	config.allowedContent = true;

	config.contentsLangDirection = 'ltr';
	//config.contentsLangDirection = 'rtl';
	//config.skin = 'moono';
	//config.skin = 'moono';
	//config.toolbar = 'full';
	//config.toolbar = 'Custom';
	/*config.toolbarGroups: [
		{ name: 'document',	   groups: [ 'mode', 'document' ] },
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'links' }
	];*/

	// FontAwesome configs
	config.contentsCss = 'view/javascript/ckeditor/plugins/fontawesome/font-awesome/css/font-awesome.min.css';
	CKEDITOR.dtd.$removeEmpty['span'] = false;
	CKEDITOR.dtd.$removeEmpty['i'] = false;
	/*config.toolbar = [
		{ name: 'insert', items: [ 'FontAwesome', 'Source' ] }
	];*/
};
