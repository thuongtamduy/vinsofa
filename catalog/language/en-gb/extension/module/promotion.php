<?php
$_['heading_title'] 				= 'Promotions';
$_['view_more_button_text']			= 'View more';
$_['description_title']				= 'Description';
$_['linked_products_title']			= 'Linked Products';
$_['linked_categories_title']		= 'Linked Categories';
$_['linked_manufacturers_title']	= 'Linked Manufacturers';

//Product page lang vars
$_['view_promotions_button'] = 'View Promotions ';
$_['promo_tab_header']		 = 'Promotions';
$_['text_no_promotions']	 = "Currently there aren't any promotions available.";


