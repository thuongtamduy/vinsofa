function sortNumber(a, b) {
	return a - b;
}

var maxHeight = function (element, add) {
	var heights = [],
		grid = $(element);

	grid.each(function () {
		$(this).css('height', 'auto');
		heights.push($(this).height());
		heights = heights.sort(sortNumber).reverse();
	});
	highest = heights[0];
	grid.each(function () {
		$(this).css('height', (highest+add));
	});

};

function refreshHeights() {
	setTimeout(function () {	 	 
		maxHeight('.caption', 8);
	}, 0);
}

$(document).ready(function () {  
  $('#grid-view, #list-view').click(function () {
  	refreshHeights();
  });
        
  $(window).resize(function () {
  	if ($("#grid-view").length) {
  		$("#grid-view").trigger( "click" );
  	} else {	
  	  refreshHeights();
  	}	
  });  
  refreshHeights();  
});	