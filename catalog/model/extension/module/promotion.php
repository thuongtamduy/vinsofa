<?php
class ModelExtensionModulePromotion extends Model {

	public function getManufacIDbyProdID($product_id){
		$query = $this->db->query('SELECT manufacturer_id FROM `'. DB_PREFIX . 'product` WHERE product_id='.$product_id)->row;

		return $query['manufacturer_id'];
	}

	public function getPromotion($promotion_id) {
		$query = $this->db->query("SELECT setting FROM " . DB_PREFIX . "module WHERE code = 'promotion' AND module_id = '".$this->db->escape($promotion_id)."' LIMIT 1");
		$data = array();

		if($query->num_rows > 0) {
			if(version_compare(VERSION, '2.1.0.0', '<')) {
        		$settings = unserialize($query->row['setting']);
        	} else {
        		$settings = json_decode($query->row['setting'], true);
        	}

        	if($settings['status'] == 'yes' && isset($settings['information_page']['status']) && $settings['information_page']['status'] == 'yes') {
        		$data = $settings;
        	}

        	if(isset($settings['general_settings']['name'][(int)$this->config->get('config_language_id')])) {
    			$data[$key]['name'] = $settings['general_settings']['name'][(int)$this->config->get('config_language_id')];
    		}

        	$data['included_products'] = $this->getAllProductsFromPromotion($settings);

		}

		return $data;
	}

	public function getPromotions(){

		$this->load->model('tool/image');

		$this->config->load('isenselabs/promotion');
		$modulePath = $this->config->get('promotion_modulePath');
		
		$query = $this->db->query("SELECT setting,module_id FROM " . DB_PREFIX . "module WHERE code = 'promotion' ");

		$data = array();

		foreach ($query->rows as $key => $value) {

			if(version_compare(VERSION, '2.1.0.0', '<')) {
        		$settings = unserialize($value['setting']);
        	} else {
        		$settings = json_decode($value['setting'], true);
        	}

        	if($settings['status'] == 'yes' && isset($settings['information_page']['status']) && $settings['information_page']['status'] == 'yes') {
        		$data[$key] = $settings;
        		$data[$key]['module_id'] = $value['module_id'];
        		$data[$key]['page_link'] = $this->url->link($modulePath.'/view', 'promotion_id='.$value['module_id'], 'SSL');

        		if(isset($settings['general_settings']['name'][(int)$this->config->get('config_language_id')])) {
        			$data[$key]['name'] = $settings['general_settings']['name'][(int)$this->config->get('config_language_id')];
        		}

        		$image_width = 268; $image_height = 180;
        		if(isset($settings['information_page']['small_image_width'])) {
					$image_width = $settings['information_page']['small_image_width'];
				}

				if(isset($settings['information_page']['small_image_height'])) {
					$image_height = $settings['information_page']['small_image_height'];
				}

        		if(isset($settings['information_page']['image']) && !empty($settings['information_page']['image'])){
        			$data[$key]['image'] = $this->model_tool_image->resize($settings['information_page']['image'], $image_width, $image_height);
        		} else {
        			$data[$key]['image'] = $this->model_tool_image->resize('no_image.png', $image_width, $image_height);
        		}
        		
        	}
		}

		return $data;

	}

	public function getTotalUses($promotion_id){
		$query = $this->db->query("SELECT customer_id FROM " . DB_PREFIX . "promotion WHERE promotion_id=".(int)$promotion_id)->rows;
	
		$uses['guests'] = 0;
		$uses['customers'] = array();
		foreach ($query as $value) {
			if($value['customer_id'] == 0) $uses['guests']++;
			else {
				$uses['customers'][] = $value['customer_id'];
			} 
		}
		$uses['total'] = $uses['guests']+count($uses['customers']);

		return $uses;
		
	}

	public function getAllProductsFromPromotion($settings) {
		$products = array();
		$categories = array();
		$manufacturers = array(); 

		foreach ($settings['conditions'] as $cond) {
			if($cond['condSelect'] == 'category_name' && isset($cond['category_id'])) $categories[] = $cond['category_id'];
			if($cond['condSelect'] == 'product_name'  && isset($cond['product_id'])) $products[] = $cond['product_id'];
			if($cond['condSelect'] == 'manufac_name'  && isset($cond['manufacturer_id'])) $manufacturers[] = $cond['manufacturer_id'];
		}

		foreach ($settings['actions'] as $action) {
			if($action['mainSelect'] == 'discount_on_products') {
				foreach ($action['product'] as $product_id) {
					$products[] = $product_id;
				}
			}

			if($action['mainSelect'] == 'discount_on_categories') {
				foreach ($action['category'] as $cat_id) {
					$categories[] = $cat_id;
				}
			}
		}

		$products 		= array_unique($products);
		$categories 	= array_unique($categories);
		$manufacturers 	= array_unique($manufacturers);

		return array('product_ids' => $products, 'category_ids' => $categories, 'manufacturer_ids' => $manufacturers);
	}

}