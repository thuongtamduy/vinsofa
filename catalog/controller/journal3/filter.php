<?php

use Journal3\Opencart\ModuleController;
use Journal3\Options\Parser;
use Journal3\Utils\Arr;

class ControllerJournal3Filter extends ModuleController {

	private static $attributes;
	private static $options;
	private static $filters;

	public function __construct($registry) {
		parent::__construct($registry);

		$this->load->language('product/category');

		$this->load->model('catalog/category');
		$this->load->model('catalog/manufacturer');
		$this->load->model('catalog/product');
		$this->load->model('journal3/filter');
	}

	public function index($args) {
		$this->module_id = (int)Arr::get($args, 'module_id');
		$this->module_type = Arr::get($args, 'module_type');

		// check routes
		if (!in_array(Arr::get($this->request->get, 'route', ''), array(
			'product/catalog',
			'product/category',
			'product/manufacturer/info',
			'product/search',
			'product/special',
		))) {
			return null;
		}

		// no products on page
		if (!Arr::get($this->request->get, 'f') && !$this->model_journal3_filter->getTotalProducts()) {
			return null;
		}

		$this->module_data = $this->model_journal3_module->get($this->module_id, $this->module_type);

		if (!$this->module_data) {
			return null;
		}

		$parser = new Parser('module/' . $this->module_type . '/general', Arr::get($this->module_data, 'general'), null, array($this->module_id));

		if ($parser->getSetting('status') === false) {
			return null;
		}

		$this->css = $parser->getCss();

		$this->settings = array_merge_recursive(
			$parser->getPhp(),
			array(
				'id'        => uniqid($this->module_type . '-'),
				'module_id' => $this->module_id,
				'classes'   => array('module', 'module-' . $this->module_type, 'module-' . $this->module_type . '-' . $this->module_id),
			),
			$this->parseGeneralSettings($parser, $this->module_id)
		);

		$this->settings['items'] = array();

		$items = Arr::get($this->module_data, 'items', array());

		foreach ($items as $item_id => $item) {
			$parser = new Parser('module/' . $this->module_type . '/item', $item, null, array($this->module_id, $item_id));

			if ($parser->getSetting('status') === false) {
				continue;
			}

			$item_settings = $this->parseItemSettings($parser, $item_id);

			if ($item_settings === null) {
				continue;
			}

			$this->css .= $parser->getCss();

			$this->settings['items'][$item_id] = array_merge_recursive(
				$parser->getPhp(),
				array(
					'classes' => array(
						'module-item',
						'module-item-' . $item_id[0],
						'module-item-' . $item_id => $item_id[0] !== $item_id,
					),
				),
				$item_settings
			);
		}

		ksort($this->settings['items'], SORT_NATURAL);

		if (isset($this->settings['items']['m'])) {
			$this->settings['items'] = array('m' => $this->settings['items']['m']) + $this->settings['items'];
		}

		if (isset($this->settings['items']['c'])) {
			$this->settings['items'] = array('c' => $this->settings['items']['c']) + $this->settings['items'];
		}

		if (isset($this->settings['items']['p'])) {
			$this->settings['items'] = array('p' => $this->settings['items']['p']) + $this->settings['items'];
		}

//		print_r(($this->settings['items'])); die;

		$this->journal3->document->addStyle('catalog/view/theme/journal3/lib/ion-rangeSlider/ion.rangeSlider.css');

		$this->journal3->document->addScript('catalog/view/theme/journal3/lib/ion-rangeSlider/ion.rangeSlider.min.js', 'footer');
		$this->journal3->document->addScript('catalog/view/theme/journal3/lib/accounting/accounting.min.js', 'footer');
		$this->journal3->document->addScript('catalog/view/theme/journal3/js/filter.js', 'footer');

		if (version_compare(VERSION, '2.2', '>=')) {
			$currency_left = $this->currency->getSymbolLeft($this->session->data['currency']);
			$currency_right = $this->currency->getSymbolRight($this->session->data['currency']);
		} else {
			$currency_left = $this->currency->getSymbolLeft();
			$currency_right = $this->currency->getSymbolRight();
		}

		$this->journal3->document->addJs(array(
			'currency_left'          => $currency_left,
			'currency_right'         => $currency_right,
			'currency_decimal'       => $this->language->get('decimal_point'),
			'currency_thousand'      => $this->language->get('thousand_point'),
			'mobileFilterButtonText' => $this->settings['mobileText'],
		));

		$this->journal3->document->addCss($this->css, "{$this->module_type}-{$this->module_id}");

		return $this->renderView('journal3/module/' . $this->module_type, $this->settings);
	}

	/**
	 * @param Parser $parser
	 * @param $module_id
	 * @return array
	 */
	protected function parseGeneralSettings($parser, $module_id) {
		$data = array();

		$data['image_width'] = $parser->getSetting('imageDimensions.width');
		$data['image_height'] = $parser->getSetting('imageDimensions.height');
		$data['image_resize'] = $parser->getSetting('imageDimensions.resize');

		return $data;
	}

	/**
	 * @param Parser $parser
	 * @param $index
	 * @return array
	 */
	protected function parseItemSettings($parser, $index) {
		$collapsed = $parser->getSetting('collapsed') === null ? $this->settings['collapsed'] : $parser->getSetting('collapsed');

		$data = array(
			'type'          => $index[0],
			'key'           => $index[0],
			'classes'       => array(
				'text-only'  => $parser->getSetting('input') !== 'select' && $parser->getSetting('display') === 'text',
				'image-only' => $parser->getSetting('input') !== 'select' && $parser->getSetting('display') === 'image',
				'panel',
			),
			'panel_classes' => array(
				'panel-collapse',
				'collapse',
			),
			'image_only'    => $parser->getSetting('display') === 'image',
		);

		switch ($index[0]) {
			case 'p':
				$price_range = $this->model_journal3_filter->getPriceRange();

				if ($price_range['min'] === $price_range['max']) {
					return null;
				}

				$data['price_range'] = $price_range;
				$data['current_price_range']['min'] = Arr::get($this->model_journal3_filter->getFilterData(), 'price.min', $price_range['min']);
				$data['current_price_range']['max'] = Arr::get($this->model_journal3_filter->getFilterData(), 'price.max', $price_range['max']);

				if ($data['current_price_range']['min'] !== $price_range['min'] || $data['current_price_range']['max'] !== $price_range['max']) {
					$collapsed = false;
				}

				break;

			case 'c':
				$categories = $this->model_journal3_filter->getCategories();


				if (!$categories) {
					return null;
				}

				foreach ($categories as $category) {
					$category['checked'] = $this->model_journal3_filter->hasFilterData('categories', $category['id']);
					$data['items'][] = $category;

					if ($category['checked']) {
						$collapsed = false;
					}
				}

				break;

			case 'm':
				$manufacturers = $this->model_journal3_filter->getManufacturers();

				if (!$manufacturers) {
					return null;
				}

				foreach ($manufacturers as $manufacturer) {
					$manufacturer['checked'] = $this->model_journal3_filter->hasFilterData('manufacturers', $manufacturer['id']);
					$data['items'][] = $manufacturer;

					if ($manufacturer['checked']) {
						$collapsed = false;
					}
				}

				break;

			case 'a':
				if (static::$attributes === null) {
					static::$attributes = $this->model_journal3_filter->getAttributes();
				}

				$attributes = Arr::get(static::$attributes, $parser->getSetting('id'));

				if (!$attributes) {
					return null;
				}

				$data['key'] .= '-' . $attributes['attribute_id'];

				foreach ($attributes['values'] as $attribute) {
					$attribute['checked'] = $this->model_journal3_filter->hasFilterData('attributes.' . $attributes['attribute_id'], $attribute['id']);
					$data['items'][] = $attribute;

					if ($attribute['checked']) {
						$collapsed = false;
					}
				}

				break;

			case 'o':
				if (static::$options === null) {
					static::$options = $this->model_journal3_filter->getOptions();
				}

				$options = Arr::get(static::$options, $parser->getSetting('id'));

				if (!$options) {
					return null;
				}

				$data['key'] .= '-' . $options['option_id'];

				foreach ($options['values'] as $option) {
					$option['checked'] = $this->model_journal3_filter->hasFilterData('options.' . $options['option_id'], $option['id']);
					$data['items'][] = $option;

					if ($option['checked']) {
						$collapsed = false;
					}
				}

				break;

			case 'f':
				if (static::$filters === null) {
					static::$filters = $this->model_journal3_filter->getFilters();
				}

				$filters = Arr::get(static::$filters, $parser->getSetting('id'));

				if (!$filters) {
					return null;
				}

				$data['key'] .= '-' . $filters['filter_group_id'];

				foreach ($filters['values'] as $filter) {
					$filter['checked'] = $this->model_journal3_filter->hasFilterData('filters.' . $filters['filter_group_id'], $filter['id']);
					$data['items'][] = $filter;

					if ($filter['checked']) {
						$collapsed = false;
					}
				}

				break;

			case 't':
				$tags = $this->model_journal3_filter->getTags();

				if (!$tags) {
					return null;
				}

				foreach ($tags as $tag) {
					$tag['checked'] = $this->model_journal3_filter->hasFilterData('tags', $tag['id']);
					$data['items'][] = $tag;

					if ($tag['checked']) {
						$collapsed = false;
					}
				}

				break;

			case 'q':
				$data['items'][] = array(
					'id'      => '1',
					'value'   => $parser->getSetting('inStockText'),
					'total'   => '',
					'checked' => $this->model_journal3_filter->hasFilterData('availability', '1'),
				);

				$data['items'][] = array(
					'id'      => '0',
					'value'   => $parser->getSetting('outOfStockText'),
					'total'   => '',
					'checked' => $this->model_journal3_filter->hasFilterData('availability', '0'),
				);

				break;

			default:
				return null;
		}

		if (in_array($index[0], array('c', 'm', 'o'))) {
			foreach ($data['items'] as &$item) {
				$image = $item['image'];

				if ($parser->getSetting('display') === 'text') {
					$item['image'] = false;
					$item['image2x'] = false;
				} else {
					if ($image) {
						$item['image'] = $this->model_journal3_image->resize($image, $this->settings['image_width'], $this->settings['image_height'], $this->settings['image_resize']);
						$item['image2x'] = $this->model_journal3_image->resize($image, $this->settings['image_width'] * 2, $this->settings['image_height'] * 2, $this->settings['image_resize']);
					} else {
						$item['image'] = $this->model_journal3_image->resize('placeholder.png', $this->settings['image_width'], $this->settings['image_height'], $this->settings['image_resize']);
						$item['image2x'] = $this->model_journal3_image->resize('placeholder.png', $this->settings['image_width'] * 2, $this->settings['image_height'] * 2, $this->settings['image_resize']);
					}
				}
			}
		}

		if ($collapsed) {
			$data['collapsed'] = true;
		} else {
			$data['collapsed'] = false;
			$data['classes'][] = 'panel-active';
			$data['panel_classes'][] = 'in';
		}

		return $data;
	}

	/**
	 * @param Parser $parser
	 * @param $index
	 * @return array
	 */
	protected function parseSubitemSettings($parser, $index) {
		return array();
	}

}
