<?php

use Journal3\Opencart\Controller;
use Journal3\Utils\Arr;

class ControllerJournal3Price extends Controller {

	public function index() {
		$this->load->model('catalog/product');
		$this->load->model('journal3/product');
		$this->load->language('product/product');

		$product_id = Arr::get($this->request->post, 'product_id');
		$product_info = $this->model_catalog_product->getProduct($product_id);

		if ($product_info) {
			$options_price = 0;

			$product_option_values = $this->model_journal3_product->getProductOptionValues($product_id, Arr::get($this->request->post, 'option', array()));
			$quantity = (int)Arr::get($this->request->post, 'quantity', 1);

			$data['in_stock'] = true;

			if ($product_info['quantity'] < 1) {
				$data['in_stock'] = false;
			}

			foreach ($product_option_values as $product_option_value) {
				if ($product_option_value['price_prefix'] === '+') {
					$options_price += $product_option_value['price'];
				}

				if ($product_option_value['price_prefix'] === '-') {
					$options_price -= $product_option_value['price'];
				}

				if ($product_option_value['subtract'] && (!$product_option_value['quantity'] || $product_option_value['quantity'] < $quantity)) {
					$data['in_stock'] = false;
				}
			}

			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$price = $product_info['price'] + $options_price;
				$data['price'] = $this->currency->format($this->tax->calculate($price, $product_info['tax_class_id'], $this->config->get('config_tax')) * $quantity, $this->session->data['currency']);
			}

			if ((float)$product_info['special']) {
				$special = $product_info['special'] + $options_price;
				$data['special'] = $this->currency->format($this->tax->calculate($special, $product_info['tax_class_id'], $this->config->get('config_tax')) * $quantity, $this->session->data['currency']);
			}

			if ($this->config->get('config_tax')) {
				$tax = (float)$product_info['special'] ? $product_info['special'] : $product_info['price'] + $options_price;
				$data['tax'] = $this->language->get('text_tax') . ' ' . $this->currency->format($tax * $quantity, $this->session->data['currency']);
			}

			foreach ($this->model_catalog_product->getProductDiscounts($product_id) as $discount) {
				$discount_price = $this->currency->format($this->tax->calculate($discount['price'] + $options_price, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				$data['discounts'][] = $discount['quantity'] . $this->language->get('text_discount') . $discount_price;
			}

			$this->renderJson('success', $data);
		} else {
			$this->renderJson('error', array(
				'message' => $this->language->get('text_error'),
			));
		}

	}

}
