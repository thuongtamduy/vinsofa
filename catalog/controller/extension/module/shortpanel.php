<?php
class ControllerExtensionModuleShortpanel extends Controller {
	public function index() {
		// Custom CSS
		$data['shortpanel_css'] = $this->config->get('module_shortpanel_css');				

        return $this->load->view('extension/module/shortpanel', $data);
	}
	public function info() {
		$this->response->setOutput($this->index());
	}
}